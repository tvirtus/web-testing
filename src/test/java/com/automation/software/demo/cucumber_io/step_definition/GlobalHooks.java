package com.automation.software.demo.cucumber_io.step_definition;

import com.automation.software.demo.cucumber_io.selenium.Driver;
import cucumber.api.Scenario;
import cucumber.api.java8.En;

public class GlobalHooks implements En {

    public GlobalHooks() {
        Before((Scenario scenario) -> Runtime
                .getRuntime()
                .addShutdownHook(new Thread(Driver::stop)));
    }
}

