Feature: Website navigation

  @nav
  Scenario Outline: Test functionality of header navigation
    Given "Home" page is displayed
    When I click header "<link text>" navigation
    Then I should be navigated to "<link text>" page
    Examples:
      | link text    |
      | Docs         |
      | Blog         |
      | Events       |
      | Talks        |
      | Videos       |
      | Learn BDD    |
      | Cucumber Pro |
      | Support      |
      | Swag         |