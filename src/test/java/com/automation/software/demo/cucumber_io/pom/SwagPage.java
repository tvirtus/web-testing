package com.automation.software.demo.cucumber_io.pom;

import com.automation.software.demo.cucumber_io.app.Page;
import com.automation.software.demo.cucumber_io.selenium.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SwagPage implements Page {

    private static final WebDriver DRIVER = Driver.getDriver();
    private final String url = "https://cucumberbdd.threadless.com/";
    private final String title = "The Cucumber Swag Shop";

    @Override
    public Page navigate() {
        DRIVER.get(url);
        return this;
    }

    @Override
    public WebDriver getDriver() {
        return DRIVER;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public boolean withCorrectTitle() {
        return DRIVER.findElement(By.cssSelector("[title=\"The Cucumber Swag Shop\"]"))
                .getText().equals(title);
    }
}
