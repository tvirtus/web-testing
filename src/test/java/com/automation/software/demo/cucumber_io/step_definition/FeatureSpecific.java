package com.automation.software.demo.cucumber_io.step_definition;

import cucumber.api.PendingException;
import cucumber.api.java8.En;

public class FeatureSpecific implements En {
    public FeatureSpecific() {
        Then("^I should see \"([^\"]*)\" ribbon on the page$", (String ribbonName) -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
        When("^I click \"([^\"]*)\" ribbon$", (String ribbonName) -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
        When("^I upload a image on the \"([^\"]*)\" section$", (String sectionName) -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
        Then("^\"([^\"]*)\" error message should display$", (String errorMessage) -> {
            // Write code here that turns the phrase above into concrete actions
            throw new PendingException();
        });
    }
}
