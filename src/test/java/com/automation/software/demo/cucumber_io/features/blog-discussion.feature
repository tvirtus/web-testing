Feature: Blog discussion

  Scenario: Test user inability to post images on the blog without logging in
    Given "Blog post" page is displayed
    When I upload a image on the "Start a discussion" section
    Then "You must be logged in to upload an image." error message should display
