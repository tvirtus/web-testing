package com.automation.software.demo.cucumber_io.app;

import com.automation.software.util.Convert;
import org.openqa.selenium.WebDriver;

public interface Page {

    /**
     * Returns instance of page based on class' name
     */
    static Page getPage(String pageName) {
        try {
            String classLocation = getClassLocation(pageName);
            Class<?> clazz = Class.forName(classLocation);
            return (Page) clazz.newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
        return null; //TODO: make it return something but null
    }

    static String getClassLocation(String pageName) {
        return String.format("com.automation.software.demo.cucumber_io.pom.%sPage",
                Convert.toPascalCase(pageName));
    }

    Page navigate();

    default boolean isDisplayed() {
            return getDriver().getCurrentUrl().equals(getUrl()) && withCorrectTitle();
    }

    WebDriver getDriver();

    String getUrl();

    boolean withCorrectTitle();

}
