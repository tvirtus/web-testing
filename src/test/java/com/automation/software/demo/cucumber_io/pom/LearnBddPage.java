package com.automation.software.demo.cucumber_io.pom;

import com.automation.software.demo.cucumber_io.app.Page;
import com.automation.software.demo.cucumber_io.selenium.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LearnBddPage implements Page {

    private static final WebDriver DRIVER = Driver.getDriver();
    private final String url = "https://cucumber.io/training";
    private final String title = "Learn BDD and Cucumber from the world's leading experts";

    @Override
    public Page navigate() {
        DRIVER.get(url);
        return this;
    }

    @Override
    public WebDriver getDriver() {
        return DRIVER;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public boolean withCorrectTitle() {
        return DRIVER.findElement(By.cssSelector(".intro-message h2"))
                .getText().equals(title);
    }
}
