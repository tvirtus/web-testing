Feature: Invest in Cucumber

  Background:
    Given "Home" page is displayed

  Scenario: Test presence of 'Invest in  Cucumber' ribbon on the home page
    Then I should see "Invest in  Cucumber" ribbon on the page

  Scenario: Assert functionality of 'Invest in  Cucumber' ribbon on home page
    When I click "Invest in  Cucumber" ribbon
    Then I should be navigated to "Invest in Cucumber" page
