package com.automation.software.demo.cucumber_io.pom;

import com.automation.software.demo.cucumber_io.app.Page;
import com.automation.software.demo.cucumber_io.selenium.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BlogPage implements Page {

    private static final WebDriver DRIVER = Driver.getDriver();
    private final String url = "https://cucumber.io/blog";
    private final String title = "The Cucumber Blog";

    @Override
    public Page navigate() {
        DRIVER.get(url);
        return this;
    }

    @Override
    public WebDriver getDriver() {
        return DRIVER;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public boolean withCorrectTitle() {
        return DRIVER.findElement(By.cssSelector("h1"))
                .getText().equals(title);
    }
}