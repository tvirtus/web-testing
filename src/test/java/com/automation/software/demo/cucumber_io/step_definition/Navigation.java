package com.automation.software.demo.cucumber_io.step_definition;

import com.automation.software.demo.cucumber_io.app.Page;
import com.automation.software.demo.cucumber_io.app.User;
import cucumber.api.java8.En;

public class Navigation implements En {

    public Navigation() {
        When("^I click header \"([^\"]*)\" navigation$", (String linkText) -> {
            Page.getPage(linkText).navigate();
        });

        Then("^I should be navigated to \"([^\"]*)\" page$", (String pageName) -> {
            Page page = Page.getPage(pageName);
            User.swear(page.isDisplayed());
        });
    }
}
