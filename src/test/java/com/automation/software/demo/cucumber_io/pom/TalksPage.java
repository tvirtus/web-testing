package com.automation.software.demo.cucumber_io.pom;

import com.automation.software.demo.cucumber_io.app.Page;
import com.automation.software.demo.cucumber_io.selenium.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TalksPage implements Page {

    private static final WebDriver DRIVER = Driver.getDriver();
    private final String url = "https://cucumber.io/talks";
    private final String title = "Conference talks";

    @Override
    public Page navigate() {
        DRIVER.get(url);
        return this;
    }

    @Override
    public WebDriver getDriver() {
        return DRIVER;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public boolean withCorrectTitle() {
        return DRIVER.findElement(By.cssSelector(".intro-message h1"))
                .getText().equals(title);
    }
}
