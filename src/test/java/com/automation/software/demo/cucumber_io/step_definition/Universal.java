package com.automation.software.demo.cucumber_io.step_definition;

import com.automation.software.demo.cucumber_io.app.Page;
import com.automation.software.demo.cucumber_io.app.User;
import cucumber.api.java8.En;

public class Universal implements En {

    public Universal() {
        Given("^\"([^\"]*)\" page is displayed$", (String pageName) -> {
            Page page = Page.getPage(pageName);
            User.swear(page.navigate().isDisplayed());
        });
    }
}
