package com.automation.software.demo.cucumber_io.selenium;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.List;

public class Driver {

    private static WebDriver driver;
    private static final int WAIT = 30;


    public static void stop() {
        if (driver != null) driver.quit();
        driver = null;
    }

    public static WebDriver getDriver() {
        if (driver == null) start();
        return driver;
    }

    public static List<WebElement> finds (By locator) {
        return (new WebDriverWait(getDriver(), WAIT).ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator)));
    }

    public static WebElement find(By locator) {
        return finds(locator).get(0);
    }

    private static void start() {
        System.setProperty("webdriver.chrome.driver", getChromeDriver());
        driver = new ChromeDriver();
        makeFullscreen();
    }

    private static void makeFullscreen(){
        driver.manage().window().fullscreen();
    }

    private static String getChromeDriver() {
        return new File(String.format("/%s/%s",
                System.getProperty("user.dir") + "/src/test/resources/drivers/",
                System.getProperty("os.name").contains("Mac") ? "chromedriver" : "chromedriver.exe"))
                .getAbsolutePath();
    }

}
