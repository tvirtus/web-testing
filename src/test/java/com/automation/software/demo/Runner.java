package com.automation.software.demo;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@nav"},
        features= "src/test/java/com/automation/software/demo/cucumber_io/features/",
        glue = {"com.automation.software.demo.cucumber_io.step_definition"},
        plugin = {"pretty", "html:target/cucumber"}
)
public class Runner {
}
