package com.automation.software.util;

import com.google.common.base.CaseFormat;

public class Convert {

    /**
     * Examples:
     * Blog -> Blog;
     * Blog post-> BlogPost;
     */
    public static String toPascalCase(String text) {
        return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, formatCase(text));
    }

    private static String formatCase(String unforrmatedClassName) {
        return unforrmatedClassName.toUpperCase()
                .replaceAll("\\d+", "")
                .replaceAll("\\W+", " ")
                .trim()
                .replaceAll("\\s+", "_");
    }
}
